function renderElement(){

    let tabsEl = document.querySelectorAll('.tabs-title');
    tabsEl.forEach((el) => {
        el.addEventListener('click',() => {

            let tabsUl  = document.querySelector('.tabs > .active');
            let elData  = el.getAttribute('data-tab_id');
            let tabTexts = document.querySelector('.tabs-content > .text_active');
            let textData  = document.querySelector('[id="'+elData+'"]');
            tabsUl.classList.remove('active');
            tabTexts.classList.remove('text_active');
            el.classList.add('active');
            textData.classList.add('text_active');

        });
    })

}

renderElement()

let addBtn = document.querySelector('.add_btn');

addBtn.addEventListener('click',() => {

    let name = prompt("Please write tab name",);
    let text = prompt("Please write tab text",);
    let parentTab = document.querySelector('.tabs')
    let newTab = document.createElement("li");
    newTab.classList.add('tabs-title');
    newTab.innerHTML = name;
    parentTab.append(newTab)

    let tabsData = document.querySelectorAll('.tabs-title');
    tabsData.forEach((el,data) => {
       el.setAttribute('data-tab_id',data);
    })

    let parentText = document.querySelector('.tabs-content')
    let newText = document.createElement("li");
    newText.innerHTML = text;
    parentText.append(newText)

    let textsData = document.querySelectorAll('.tabs-content > li');
    textsData.forEach((textEl,textId) => {
        textEl.setAttribute('id',textId);
    })

    renderElement()
})